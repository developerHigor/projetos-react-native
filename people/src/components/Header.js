import React from 'react';
import { View , Text, StyleSheet} from 'react-native';

const style = StyleSheet.create({
    view: {
        marginTop: 30,
        backgroundColor: "#6ca2f7",
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 50,
        color: '#fff'
    }
});

const Header = (props) =>(
    <View style={style.view}>
        <Text style={style.title}>{props.title}</Text>
    </View>
);

export default Header;
