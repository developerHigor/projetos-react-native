import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {capitalizeFirstLetter} from '../utils';

const styles = StyleSheet.create({
    line: {
        flexDirection: 'row',
        alignItems: "center",
        height: 60,
        borderBottomWidth: 1,
        borderBottomColor: '#bbb'
    },
    lineText: {
        fontSize: 20,
        paddingLeft: 15,
        flex: 7
    },
    avatar:{
        aspectRatio: 1,
        flex: 1,
        marginLeft: 15,
        borderRadius: 50
    }
})

const PeopleListItem = props => {
    const { people, navegateToPeopleDetail } = props;
    const { title, first, last } = people.name;
    
    return (
        <TouchableOpacity onPress={() => navegateToPeopleDetail({ people }) }>
            <View style={styles.line}>
                <Image style={styles.avatar} source={ { uri: people.picture.thumbnail} } />
                <Text style={styles.lineText}>
                    { 
                    `${
                            capitalizeFirstLetter(title)
                        } ${
                            capitalizeFirstLetter(first)
                        } ${
                            capitalizeFirstLetter(last)
                        }` 
                    }
                </Text>
            </View>
        </TouchableOpacity>
    )
};

export default PeopleListItem;
