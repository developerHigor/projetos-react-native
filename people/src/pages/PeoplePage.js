import React from 'react';
import {  View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import axios from 'axios';
import PeopleList from '../components/PeopleList';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  error: {
    fontSize: 20,
    color: 'red',
    alignSelf: 'center'
  }
})

export default class PeoplePage extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      peoples: [],
      loading: false,
      error: false,
      messageError: ''
    }
  }

  componentDidMount(){
    this.setState({ loading: true });
      axios
        .get("https://randomuser.me/api/?nat=br&results=150")
        .then(response =>{
          const {results} = response.data;
          this.setState({
            peoples: results,
            loading: false
          });
        }).catch( error => {
          this.setState({ 
            loading: false,
            error: true,
            messageError: error.message,
          })
          console.log(error.message)
        });
  }
  
  render(){
    return (
      <View style={styles.container}> 
        {
          this.state.loading ? 
            <ActivityIndicator size="large" color="#6ca2f7" /> 
            : this.state.error ? 
              <Text style={styles.error}>Ops, erro ao buscar os dados: {"\n"}{this.state.messageError}</Text>
              : <PeopleList 
                  peoples={this.state.peoples} 
                  onPressItem={ pageParams => this.props.navigation.navigate('PeopleDetail', pageParams) }
                />
        }
      </View>

    );
  }
}
