import React from "react";
import { View, Text, Image, StyleSheet } from 'react-native';
import Line from '../components/Line'


const styles = StyleSheet.create({
    avatar:{
        aspectRatio: 1,
        width:300,
        borderRadius: 50
    },
    container: {
        flexDirection: 'row',
        alignItems: "center",
        padding: 15,
        display: 'flex',
        justifyContent: 'center'
    },
    detailContainer: {
        backgroundColor: '#e2f9ff',
        padding: 15,
        elevation: 1
    }
})

export default class PeopleDetailPage extends React.Component{
    render() {
        const { people } = this.props.navigation.state.params;
        return (
            <View>
                <View style={ styles.container }>
                    <Image style={styles.avatar} source={ { uri: people.picture.large}} />
                </View>
                <View style={styles.detailContainer}>
                    <Line label='Email:' content={people.email} />
                    <Line label='Cidade:' content={people.location.city} />
                    <Line label='Estado:' content={people.location.state} />
                    <Line label='Nacionalidade:' content={people.nat} />
                    <Line label='Tel:' content={people.phone} />
                    <Line label='Cel:' content={people.cell} />
                </View>
            </View>
        );
    }
}
