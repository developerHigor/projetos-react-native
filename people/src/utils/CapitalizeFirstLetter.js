/*
    Deixa a primeira letra da palavra em maíusculo
    Ex: exemplo -> Exemplo
*/

const capitalizeFirstLetter = string => string[0].toUpperCase() + string.slice(1);
export default capitalizeFirstLetter;
