import React from "react";
import Router from './Router';
import rootReducer from './reducers';

import { createStore, compose, applyMiddleware } from 'redux';
import {  Provider } from "react-redux";
import reduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

const composeEnhancer = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;
const store = createStore(rootReducer, 
    composeWithDevTools(applyMiddleware(reduxThunk))
)

// const store = createStore(rootReducer, 
//     composeEnhancer(applyMiddleware(thunk))
// )

const SeriesApp = props => (
    <Provider store={store}>
        <Router />
    </Provider>
)

export default SeriesApp;
