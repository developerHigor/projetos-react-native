import firebase from "firebase/compat/app";
import { Alert } from "react-native";

export const SET_SERIES = 'SET_SERIES';
const setSeries = series =>({
    type: SET_SERIES,
    series
})

export const watchSeries = () => {
    const { currentUser } = firebase.auth();
    return dispatch => {
        return firebase
            .database()
            .ref(`/users/${currentUser.uid}/series/`)
            .on('value', snapshot =>{
                dispatch(setSeries(snapshot.val()))
            });
    }
}


export const DELETE_SERIE = 'DELETE_SERIE';
export const deleteSerie = serie =>{
    return dispatch => {
        return new Promise((resolve, reject) => {
            Alert.alert(
                "Deletar", 
                `Deseja deletar a série ${serie.title}`,
                [{
                    text: "Não",
                    onPress: () => {
                        resolve(false);
                    }
                },{
                    text: "Sim",
                    onPress: async () => {
                        try {
                            const { currentUser } = firebase.auth();
                            const db = firebase.database();
                            await db.ref(`/users/${currentUser.uid}/series/${serie.id}`).remove();
                            resolve(true);
                        } catch (error) {
                            reject(error);
                        }
                    }
                }],
                { cancelable: false }
            )
        })
    }
}
