import { combineReducers } from "redux";
import serieFormReducer from "./serieFormReducer";
import serieReducer from "./seriesReducer";
import userReducer from "./userReducer";

export default combineReducers({
    user: userReducer,
    serieForm: serieFormReducer,
    series: serieReducer
})
