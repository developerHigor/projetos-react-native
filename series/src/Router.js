import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LoginScreen from './pages/LoginPage';
import SeriesPage from './pages/SeriesPage';
import SerieDetailPage from './pages/SerieDetailPage';
import SerieFormPage from './pages/SerieFormPage';

const AppNavigator = createStackNavigator({
  'Login': {
    screen: LoginScreen,
    navigationOptions: {
      title: "Bem Vindo"
    }
  },
  'Main': {
    screen: SeriesPage
  },
  'SerieDetail': {
    screen: SerieDetailPage,
    navigationOptions: ({navigation}) => {
      const { serie } = navigation.state.params;
      return {
        title: serie.title,
        headerTitleStyle: {
          color: 'white',
          fontSize: 25,
          flexGrow: 1,
        }
      }
    }
  },
  'SerieForm': {
    screen: SerieFormPage,
    navigationOptions: ({ navigation }) => {
      let titulo = 'Nova Série';
      const { params } = navigation.state;
      if(params && params?.serieToEdit)
        titulo = params?.serieToEdit.title;

      return  {
        title: titulo,
        headerTitleStyle: {
          color: 'white',
          fontSize: 25,
          flexGrow: 1,
        }
      }
    }
  }
},{
  defaultNavigationOptions: {
    title: 'Séries',
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#6ca2f7',
      borderBottomWidth: 1,
      borderBottomColor: '#c5c5c5'
    },
    headerTitleStyle: {
      color: 'white',
      fontSize: 25,
      flexGrow: 1,
      textAlign: 'center'
    }
  }
});

const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;
