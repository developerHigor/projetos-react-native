import React from "react";
import { View, Text, FlatList, StyleSheet, ActivityIndicator } from "react-native";
import SerieCard from "../components/SerieCard";
import AddSerieCard from "../components/AddSerieCard"
import { connect } from 'react-redux';
import { watchSeries } from "../actions/seriesActions";

const isEvent = valor => valor % 2 === 0;

const styles = StyleSheet.create({
    marginTop: {
        marginTop: 5
    },
    marginBottom: {
        marginBottom: 5
    },
})

class SeriesPage extends React.Component {

    componentDidMount() {
        this.props.dispatchWatchSeries();
    }

    render(){
        const { series,  navigation } = this.props;
        if(!series)
            return (<ActivityIndicator size={'large'} />)

        return(
            <View>
                <FlatList 
                    data={ [...series, { isLast: true }] }
                    renderItem={({item, index}) => (
                        item?.isLast 
                        ? <AddSerieCard 
                            isFirstComun={isEvent(index)}
                            onNavigate={() => navigation.navigate('SerieForm')}
                        />
                        :<SerieCard 
                            serie={item} 
                            isFirstComun={isEvent(index)}
                            onNavigate={() => navigation.navigate('SerieDetail', { serie: item })}
                        />
                    )}
                    keyExtractor={item => item.id}
                    numColumns={2}
                    ListHeaderComponent={props => (<View style={styles.marginTop} />)}
                    ListFooterComponent={props => (<View style={styles.marginBottom} />)}
                />
            </View>
        )
    }
}

const mapStateToProps = state => {
    let { series } = state;
    series = series || [];
    
    if(!series)
        return series;

    const keys = Object.keys(series);
    const seriesWithKeys = keys.map(id =>{
        return {...series[id], id}
    })

    return { series: seriesWithKeys };
}

export default connect(
    mapStateToProps,
    { dispatchWatchSeries: watchSeries }
)(SeriesPage);
