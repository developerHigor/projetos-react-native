import React from "react";
import { 
    View, 
    ScrollView, 
    StyleSheet, 
    Image,
    Button,
    Alert,
 } from 'react-native';
import Line from "../components/Line";
import LongLine from "../components/LongLine";
import { connect } from 'react-redux';
import { deleteSerie } from "../actions";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        alignSelf: 'center',
        width: '90%',
    },
    img: {
        aspectRatio: 1,
        marginBottom: 10
    },
    button: {
        marginTop: 20
    }
})

class SerieDetailPage extends React.Component{
    render() {
        const navigation = this.props.navigation;
        const { serie } = navigation.state.params;
        return(
            <ScrollView style={styles.container}>
                <Image 
                    style={styles.img}
                    source={{uri: `data:image/jpeg;base64,${serie?.img64}`}}
                />
                <Line label="Título" content={serie.title} />
                <Line label="Gênero" content={serie.gender} />
                <Line label="Nota" content={serie.rate} />
                <LongLine label="Descrição" content={serie.description} />
                <View style={styles.button}>
                    <Button 
                        title="Editar"
                        onPress={() => {navigation.replace('SerieForm', { serieToEdit: serie })}}
                    />
                </View>
                <View style={styles.button}>
                    <Button 
                        color="#FF0004FF"
                        title="Deletar"
                        onPress={ async () => {
                            const hasDeleted = await this.props.deleteSerie(serie);
                            if(hasDeleted)
                                navigation.goBack();
                        }}
                    />
                </View>
            </ScrollView>
        )
    }
}



export default connect(null, { deleteSerie })(SerieDetailPage);
