import React from "react";
import { 
    View, 
    Text, 
    TextInput, 
    Button, 
    StyleSheet, 
    ActivityIndicator
 } from "react-native";
import FormRow from "../components/FormRow";
import  firebase from "firebase/compat/app";
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { tryLogin } from "../actions";
import { connect } from "react-redux";

//Alert para web
//npm i react-native-awesome-alerts

const styles = StyleSheet.create({
    input: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'gray',
        padding: 10
    },
    title: {
        fontSize: 25,
        color: '#6ca2f7',
        alignSelf: 'center',
        paddingBottom: 10,
        fontWeight: 'bold'
    },
    containerLogin: {
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10
    },
    toastError: {
        marginTop: 15,
        color: 'red',
        fontWeight: 'bold',
        fontSize: 18,
        alignSelf: 'center'
    },
    toastSuccess: {
        marginTop: 15,
        color: 'green',
        fontWeight: 'bold',
        fontSize: 18,
        alignSelf: 'center'
    }
})

class LoginScreen extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            email: '',
            password: '',
            loading: false,
            message: ''
        }
    }

    componentDidMount(){
        // Your web app's Firebase configuration
        // For Firebase JS SDK v7.20.0 and later, measurementId is optional
        const firebaseConfig = {
            apiKey: "AIzaSyBK7XqXAY9N_HecXKtHz7hlp-a7qjjCtiI",
            authDomain: "series-92c35.firebaseapp.com",
            projectId: "series-92c35",
            storageBucket: "series-92c35.appspot.com",
            messagingSenderId: "978017691777",
            appId: "1:978017691777:web:4a9204ac55f535a7a44580",
            measurementId: "G-VQW6DCG9HR"
        };
        
        firebase.initializeApp(firebaseConfig);
    }

    onChangeInput(field, value){
        this.setState({ [field]: value })
    }

    async trylogin() {
        this.setState({ loading: true, message: '' })
        const { email, password } = this.state;

        this.props.tryLogin({email, password})
            .then(user => {
                if(user)
                    return this.props.navigation.replace('Main');

                this.setState({ 
                    loading: false,
                    message: ''
                })
            }).catch(error =>{
                this.setState({ 
                    loading: false,
                    message: this.getMessageError(error)
                })
            });
    }

    getMessageError(error){
        switch(error.code){
            case 'auth/wrong-password':
                return 'Senha incorreta';
            case 'auth/user-not-found':
                return 'Usuário não encontrado';
            case 'auth/invalid-email':
                return 'E-mail informado incorretamente';
            default:
                return `Erro desconhecido: ${error.message}\nCodigo: ${error.code}`;
        }
    }

    renderButtonLogin(){
        if(this.state.loading)
            return <ActivityIndicator />
        return <Button 
                    title="Entrar"
                    onPress={() => this.trylogin()}
                />
    }

    renderMessage(){
        const {message} = this.state
        if(!message)
            return null;

        if(!message.toUpperCase().includes('SUCESSO')){
            return (<Text style={styles.toastError}>{message}</Text>);
        }
        return (<Text style={styles.toastSuccess}>{message}</Text>);
    }

    render(){
        return (
            <View style={styles.container}>
                <View style={styles.containerLogin}>
                    <Text style={styles.title}>Login</Text>
                    <FormRow>
                        <TextInput
                            style={styles.input}
                            placeholder="email@email.com.br" 
                            value={this.state.email}
                            onChangeText={value => this.onChangeInput('email', value) }
                            keyboardType="email-address"
                        />
                    </FormRow>
                    <FormRow>
                        <TextInput
                            style={styles.input}
                            secureTextEntry
                            placeholder="******" 
                            onChangeText={value => this.onChangeInput('password', value) }
                        />
                    </FormRow>
                    { this.renderButtonLogin() }
                    { this.renderMessage() }
                </View>
            </View>
        )
    }
}

export default connect(null, { tryLogin })(LoginScreen)
