import React from "react";
import { 
    View, 
    Text, 
    StyleSheet,
    TextInput,
    Picker,
    Button,
    ScrollView,
    KeyboardAvoidingView,
    ActivityIndicator,
    Alert,
    Image
 } from 'react-native';
import FormRow from '../components/FormRow'
import Slider from '@react-native-community/slider';

import { connect } from "react-redux";
import { 
    saveSerie, 
    setField, 
    setWholeSerie,
    resetForm,
} from "../actions";

import * as Permissions from "expo-permissions";
import * as ImagePicker from 'expo-image-picker';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        alignSelf: 'center',
        width: '90%',
    },
    input: {
        marginBottom: 5,
        marginLeft: 5,
        marginRight: 5
    },
    sameRow:{
        fontSize: 15,
        fontWeight: 'bold',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    }, 
    img: {
		aspectRatio: 1,
		width: '100%',
	},
    button: {
        marginTop: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    buttonSave: {
        marginBottom: 25,
        paddingLeft: 15,
        paddingRight: 15
    }
})

const TipoImagem = {
    Camera: 'Camera',
    Biblioteca: 'Biblioteca'
}

class SerieFormPage extends React.Component { 
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false
        }
    }

    componentDidMount() {
        const { navigation, dispatchSetWholeSerie, dispatchResetForm } = this.props;
        const { params } = navigation.state;

        if(params && params.serieToEdit)
            dispatchSetWholeSerie(params.serieToEdit)
        else
            dispatchResetForm()
    }

    async pickImage(tipoImagem){
        const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL)
        if(status !== 'granted'){
            console.log("Usuário não permitiu");
            Alert.alert("Premissão", "Você precisa permitir o acesso para podermos utilizar a câmera.")
            return;
        }

        let result = null;
        if(tipoImagem == TipoImagem.Camera){
            result = await ImagePicker.launchCameraAsync({
                quality: 0.2,
                base64: true,
                allowsEditing: true,
                aspect: [1, 1]
            })
        }else{
            result = await ImagePicker.launchImageLibraryAsync({
                quality: 0.2,
                base64: true,
                allowsEditing: true,
                aspect: [1, 1]
            })
        }

        if(!result.cancelled){
            this.props.dispatchSetField('img64', result.base64)
            return;
        }
    }

    render(){
        const {
            serieForm, 
            dispatchSetField, 
            dispatchSalvarSerie,
            navigation
        } = this.props;

        return(
            <KeyboardAvoidingView 
                //behavior="padding" 
                enabled
                //keyboardVerticalOffset={5}
            >
                <ScrollView>
                    <FormRow first>
                        <TextInput 
                            style={styles.input}
                            placeholder='Título'
                            value={serieForm.title}
                            onChangeText={value => dispatchSetField('title', value)}
                        />
                    </FormRow>

                    <FormRow>
                        {
                            serieForm.img64
							? <Image
								source={{
									uri: `data:image/jpeg;base64,${serieForm.img64}`
								}}
								style={styles.img} />
							: null 
                        }
                        <View style={styles.button}>
                            <Button title="Tirar foto" onPress={() => this.pickImage(TipoImagem.Camera)} />
                        </View>
                        <View style={styles.button}>
                            <Button title="Selecione uma imagem" onPress={() => this.pickImage(TipoImagem.Biblioteca) } />
                        </View>
                    </FormRow>
                    <FormRow>
                        <Picker 
                            selectedValue={serieForm.gender}
                            onValueChange={itemValue => dispatchSetField('gender', itemValue)}
                        >
                            <Picker.Item label="Terror" value="horror" />
                            <Picker.Item label="Comédia" value="comedy" />
                            <Picker.Item label="Ação" value="action" />
                        </Picker>
                    </FormRow>
                    <FormRow>
                        <View style={styles.sameRow}>
                            <Text>Nota:</Text>
                            <Text>{serieForm.rate}</Text>
                        </View>
                    </FormRow>
                    <FormRow>
                        <Slider 
                            onValueChange={value => dispatchSetField('rate', value)}
                            value={serieForm.rate}
                            minimumValue={0}
                            maximumValue={100}
                            step={5}
                        />
                    </FormRow>
                    <FormRow first>
                        <TextInput 
                            style={styles.input}
                            placeholder='Descrição'
                            value={serieForm.description}
                            onChangeText={value => dispatchSetField('description', value)}
                            numberOfLines={6}
                            multiline={true}
                        />
                    </FormRow>
                    {
                        this.state.isLoading ? 
                        <ActivityIndicator />
                        : (
                            <View style={styles.buttonSave}>
                                <Button
                                    title="Salvar"
                                    onPress={ async () => {
                                        try {
                                            this.setState({ isLoading: true })
                                            await dispatchSalvarSerie(serieForm);
                                            navigation.goBack();
                                        } catch (error) {
                                            Alert.alert("Erro!", error.message)
                                        }finally{
                                            this.setState({ isLoading: false })
                                        }
                                    }}
                                />
                            </View>
                        )
                    }
                    
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }

}

const mapStateToProps = state => {
    return{
        serieForm: state.serieForm
    }
}

const mapDispatchToProps = {
    dispatchSetField: setField,
    dispatchSalvarSerie: saveSerie,
    dispatchSetWholeSerie: setWholeSerie,
    dispatchResetForm: resetForm
}

export default connect(
    mapStateToProps, 
    mapDispatchToProps
)(SerieFormPage);
