import React from "react";
import { View, Platform, Text, StyleSheet, TouchableWithoutFeedback, LayoutAnimation, NativeModules } from 'react-native';

const styles = StyleSheet.create({
    line: {
        paddingTop: 3,
        paddingBottom: 3,
    },
    cell: {
        fontSize: 16,
        paddingLeft: 5
    },
    label: {
        fontWeight: 'bold',
        flex: 1,
        paddingBottom: 10
    },
    content: {
        flex: 3,
        textAlign: 'justify'
    },

    collapsed:{
        maxHeight: 65
    },
    expanded: {
        flex: 1
    }
})

if (Platform.OS === 'android' && NativeModules.UIManager.setLayoutAnimationEnabledExperimental) {
    NativeModules.UIManager.setLayoutAnimationEnabledExperimental(true);
}

export default class LongLine extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            isExpanded: false
        } 
    }

    componentWillUpdate(nextProps, nextState){
        LayoutAnimation.spring();
    }

    toogleIsExpanded(){
        const { isExpanded } = this.state;
        this.setState({
            isExpanded: !isExpanded
        })
    }

    render(){
        const {label, content} = this.props;
        const { isExpanded } = this.state;
        return(
            <View style={styles.line}>
                <Text style={ [styles.cell, styles.label ] }>{ label !== undefined ? label : '-' }</Text>
                <TouchableWithoutFeedback onPress={() => this.toogleIsExpanded()}>
                    <View >
                        <Text style={ [ styles.cell,  styles.content, isExpanded ? styles.expanded : styles.collapsed ] }>
                            { content !== undefined ? content : '-' }
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}
