import React from "react";
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
    container: {
        width: '50%',
        padding: 5,
        height: Dimensions.get('window').width / 2
    },
    card:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
    },
    img:{
        width: '80%',
        height: '80%'
    },
    firstColumn: {
        paddingLeft: 10,
    },
    lastColumn: {
        paddingRight: 10
    }
})

const AddSerieCard = ({serie, isFirstComun, onNavigate}) => (
    <TouchableOpacity 
        onPress={onNavigate}
        style={[ 
                styles.container,
                isFirstComun ? styles.firstColumn : styles.lastColumn
        ]}
     >
        <View style={styles.card}>            
            <Image 
                style={styles.img}
                source={
                    require('../../resources/add.png')
                }
            />
        </View>
    </TouchableOpacity>
)

export default AddSerieCard;
