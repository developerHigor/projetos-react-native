import React from "react";
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
    container: {
        //flex: .5,        
        width: '50%',
        padding: 5,
        height: Dimensions.get('window').width / 2
    },
    card:{
        flex: 1,
        borderWidth: 1
    },
    cardTitleWrapper:{
        marginTop: 10,
        backgroundColor: 'black',
        height: 40,
        width: '100%',

        position: 'absolute',
        bottom: 0,
        opacity: 0.8,
        alignItems: 'center',

        paddingTop: 10,
        paddingBottom: 10,

        paddingLeft: 5,
        paddingRight: 5
    },
    cardTitle:{
        color: 'white',
        fontSize: 12,
        fontWeight: 'bold'
    },
    img:{
        aspectRatio: 1,
        resizeMode: 'cover'
    },
    firstColumn: {
        paddingLeft: 10,
    },
    lastColumn: {
        paddingRight: 10
    }
})

const SerieCard = ({serie, isFirstComun, onNavigate}) => (
    <TouchableOpacity 
        onPress={onNavigate}
        style={[ 
                styles.container,
                isFirstComun ? styles.firstColumn : styles.lastColumn
        ]}
     >
        <View style={styles.card}>            
            <Image 
                style={styles.img}
                source={
                    { uri: `data:image/jpeg;base64,${serie?.img64}`}
                }
            />
            <Text style={[ styles.cardTitleWrapper, styles.cardTitle] }>{serie?.title}</Text>
        </View>
    </TouchableOpacity>
)

export default SerieCard;
