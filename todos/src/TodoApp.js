import React from "react";
import { View, StyleSheet } from "react-native";
import { createStore, applyMiddleware } from "redux";
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import TodoForm from "./components/TodoForm";
import TodoList from "./components/TodoList";
import rootReducer from "./reducers";
import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(rootReducer,
    composeWithDevTools(
        applyMiddleware(logger)
    )
);

const styles = StyleSheet.create({
    container:{
        marginTop: 30
    }
})

export default class TodoApp extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    <TodoForm />
                    <TodoList />
                </View>
            </Provider>
        )
    }
}
