import React from "react";
import { TextInput, StyleSheet } from "react-native";

const styles = StyleSheet.create({
    input: {
        padding: 10,
        elevation: 1
    }
})

const Input = ({onChangeText, value}) => (
    <TextInput 
        style={styles.input}
        onChangeText={onChangeText}
        value={value}
        underlineColorAndroid="#000"
     />
);

export default Input;
