import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import Input from './Input';
import { connect } from "react-redux";
import { setTodoText, addTodo, updateTodo } from "../actions";


const styles = StyleSheet.create({
    formContainer: {
        flexDirection: 'row',
    },
    inputContainer:{
        flex: 4
    },
    buttonContainer: {
        flex: 1
    }
})

class TodoForm extends React.Component{
    onChangeText(text){
        this.props.dispatchSetTodoText(text);
    }

    onPress(){
        const { todo } = this.props;
        if(todo.id)
            return this.props.dispatchUpdateTodo(todo)

        const {text} = this.props.todo;
        return this.props.dispatchAddTodo(text)
    }

    render(){
        const { todo } = this.props;
        const {text} = todo;
        return(
            <View style={styles.formContainer}>
                <View style={styles.inputContainer}>
                    <Input 
                        onChangeText={text => this.onChangeText(text)}
                        value={text}
                    />
                </View>
                <View style={styles.buttonContainer}>
                    <Button 
                        onPress={() => this.onPress()} 
                        title={ todo.id ? "Save" : "Add"}
                    />
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        todo: state.todo
    }
}

/* mapStateToProps, mapDispatchToProps */
export default connect(
    mapStateToProps, 
    {
        dispatchAddTodo: addTodo,
        dispatchSetTodoText: setTodoText,
        dispatchUpdateTodo: updateTodo
    }
)(TodoForm);
