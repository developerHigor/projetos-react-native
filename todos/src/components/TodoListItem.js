import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
    line: {
        flexDirection: 'row',
        alignItems: "center",
        height: 60,
        borderBottomWidth: 1,
        borderBottomColor: '#bbb'
    },
    lineText: {
        fontSize: 20,
        paddingLeft: 15,
        flex: 7
    },
    lineThrough:{
        textDecorationLine: 'line-through'
    }
})

const TodoListItem = ({todo, onPressTodo, onLongPressTodo}) => {
    return (
        <TouchableOpacity 
            onPress={onPressTodo}
            onLongPress={onLongPressTodo}
        >
            <View style={styles.line}>
                <Text style={[ styles.lineText, todo.done ? styles.lineThrough : null ]}>
                    {todo.text}
                </Text>
            </View>
        </TouchableOpacity>
    )
};

export default TodoListItem;
